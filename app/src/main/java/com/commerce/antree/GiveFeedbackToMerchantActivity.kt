package com.commerce.antree

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.commerce.antree.ui.givefeedbacktomerchant.GiveFeedbackToMerchantFragment

class GiveFeedbackToMerchantActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.give_feedback_to_merchant_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, GiveFeedbackToMerchantFragment.newInstance())
                .commitNow()
        }
    }
}