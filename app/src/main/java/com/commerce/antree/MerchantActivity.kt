package com.commerce.antree

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.commerce.antree.ui.main.merchant.MerchantFragment

class MerchantActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.merchant_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MerchantFragment.newInstance())
                .commitNow()
        }
    }
}