package com.commerce.antree

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.commerce.antree.ui.merchantreviewed.MerchantReviewedFragment

class MerchantReviewedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.merchant_reviewed_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MerchantReviewedFragment.newInstance())
                .commitNow()
        }
    }
}