package com.commerce.antree

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.commerce.antree.ui.verifyphonenumber.VerifyPhoneNumberFragment

class VerifyPhoneNumberActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.verify_phone_number_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, VerifyPhoneNumberFragment.newInstance())
                .commitNow()
        }
    }
}