package com.commerce.antree.appintro

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.commerce.antree.R
import kotlinx.android.synthetic.main.activity_app_intro.*
import android.view.View
import android.widget.Toast
import com.commerce.antree.HomeActivity
import com.commerce.antree.LoginActivity
import com.commerce.antree.RegisterActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging

class AppIntroActivity: AppCompatActivity() {

        var pageSelected: Int = 0

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_app_intro)

            viewpager_appintro_slider.adapter = AppIntroAdapter(supportFragmentManager)
            viewpager_appintro_slider.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {}
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                    // Check if this is the page you want.
                    pageSelected = position

                    if (position == 1) {
                        pageSelected = position
                        txt_appintro_lastslider.visibility = View.VISIBLE
                        txt_appintro_nextslider.visibility = View.VISIBLE
                    } else if (position == 2) {
                        txt_appintro_lastslider.visibility = View.VISIBLE
                        txt_appintro_nextslider.text = "START"
                    } else {
                        txt_appintro_lastslider.visibility = View.GONE
                        txt_appintro_nextslider.visibility = View.VISIBLE
                    }
                }
            })

            indicator_appintro_slider.setViewPager(viewpager_appintro_slider)

            txt_appintro_nextslider.setOnClickListener {
                if (pageSelected == 2) {
                    val intent = Intent(this, RegisterActivity::class.java)
                    startActivity(intent)
                }
            }

            //FirebaseMessaging.getInstance().isAutoInitEnabled = true

            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w(TAG, "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token

                    // Log and toast
                    val msg = getString(R.string.msg_token_fmt, token)
                    Log.d(TAG, msg)
                    //Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                })
        }

    companion object {
        val TAG = "Firebase Token"
    }
}
