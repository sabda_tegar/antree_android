package com.commerce.antree.appintro

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.commerce.antree.R

class AppIntroAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm!!) {

    private val titles = listOf(
        "Selalu terapkan protokol kesehatan dengan Antree",
        "Segala Tempat, di Satu App",
        "Ngantri-nya dari rumah aja !"
    )

    private val images = listOf(
        R.drawable.appintro_family,
        R.drawable.appintro_city,
        R.drawable.appintro_time
    )

    private val descriptions = listOf(
        "Antree menjadi pengingat untuk selalu menerapkan protokol kesehatan setiap kali anda keluar rumah. Yuk selalu terapkan protokol kesehatan untuk melindungi diri anda dan keluarga tercinta.",
        "Informasi penerapan protokol kesehatan di segala tempat kini dalam genggamanmu. Ga perlu repot lagi cari - cari info, nanya temen, ataupun ngintip story di akun IG-nya. Bepergian pun kini lebih tenang.",
        "Lewat Antree, daftar antrian kini bisa dari rumah, gak perlu lagi datang ke lokasi, desak - desakan nungguin yang gak pasti. Datang nya nanti aja kalo antriannya udah dipanggil, jadi hemat waktu dan tetap bisa menerapkan social distancing."
    )

    override fun getItem(p0: Int): Fragment {
        return AppIntroFragment.newInstance(titles[p0], descriptions[p0], images[p0])!!
    }

    override fun getCount(): Int {
        return titles.size
    }

}