package com.commerce.antree.appintro

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.commerce.antree.R
import kotlinx.android.synthetic.main.fragment_app_intro.*

class AppIntroFragment: Fragment() {

    lateinit var titles: String
    lateinit var descs: String
    var images: Int? = null
    lateinit var baseView: View

    companion object {
        fun newInstance(title: String, description: String, image: Int) : Fragment? {
            val bundle = Bundle()
            bundle.putString("title", title)
            bundle.putString("desc", description)
            bundle.putInt("image", image)

            val fragment = AppIntroFragment()
            fragment.setArguments(bundle)

            return fragment
        }
    }

    private fun readBundle(bundle: Bundle?) {
        if (bundle != null) {
            titles = bundle.getString("title").toString()
            descs = bundle.getString("desc").toString()
            images = bundle.getInt("image")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_app_intro, container, false)
    }

    @SuppressLint("NewApi")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        readBundle(arguments)

        txt_appintro_title.text = titles
        txt_appintro_description.text = descs
        context?.let {
            Glide.with(it)
                .load(images)
                .into(img_appintro_illustration)
        }
    }
}
