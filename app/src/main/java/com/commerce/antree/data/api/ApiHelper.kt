package com.commerce.antree.data.api

import com.commerce.antree.data.model.place.Merchant

class ApiHelper (private val apiService: ApiService) {
    suspend fun getPlaces() = apiService.getPlaces()
    suspend fun getPlaceByID(merchantId: String) = apiService.getPlaceByID(merchantId)
}