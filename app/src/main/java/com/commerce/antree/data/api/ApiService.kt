package com.commerce.antree.data.api

import com.commerce.antree.data.model.place.Base
import com.commerce.antree.data.model.place.Merchant
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("api/get/place/all")
    suspend fun getPlaces(): Base

    @GET("api/get/place/{id}")
    suspend fun getPlaceByID(@Path("id") merchantId: String): Merchant
}