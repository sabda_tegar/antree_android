package com.commerce.antree.data.model.place

import com.google.gson.annotations.SerializedName

/*
Copyright (c) 2020 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */


data class Base (

	@SerializedName("current_page") val current_page : Int,
	@SerializedName("data") val merchant : List<Merchant>,
	@SerializedName("first_page_url") val first_page_url : String,
	@SerializedName("from") val from : Int,
	@SerializedName("last_page") val last_page : Int,
	@SerializedName("last_page_url") val last_page_url : String,
	@SerializedName("next_page_url") val next_page_url : String,
	@SerializedName("path") val path : String,
	@SerializedName("per_page") val per_page : Int,
	@SerializedName("prev_page_url") val prev_page_url : String,
	@SerializedName("to") val to : Int,
	@SerializedName("total") val total : Int
)