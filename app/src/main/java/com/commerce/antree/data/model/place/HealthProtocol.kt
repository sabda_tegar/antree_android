package com.commerce.antree.data.model.place

import com.google.gson.annotations.SerializedName

/*
Copyright (c) 2020 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */


data class HealthProtocol (

	@SerializedName("id") val id : Int,
	@SerializedName("place_id") val place_id : Int,
	@SerializedName("commitment_1_text") val commitment_1_text : String,
	@SerializedName("commitment_2_text") val commitment_2_text : String,
	@SerializedName("commitment_3_text") val commitment_3_text : String,
	@SerializedName("commitment_4_text") val commitment_4_text : String,
	@SerializedName("commitment_5_text") val commitment_5_text : String,
	@SerializedName("commitment_1") val commitment_1 : Int,
	@SerializedName("commitment_2") val commitment_2 : Int,
	@SerializedName("commitment_3") val commitment_3 : Int,
	@SerializedName("commitment_4") val commitment_4 : Int,
	@SerializedName("commitment_5") val commitment_5 : Int,
	@SerializedName("file") val file : String,
	@SerializedName("note") val note : String,
	@SerializedName("created_at") val created_at : String,
	@SerializedName("updated_at") val updated_at : String,
	@SerializedName("commitment_1_image") val commitment_1_image : String,
	@SerializedName("commitment_2_image") val commitment_2_image : String,
	@SerializedName("commitment_3_image") val commitment_3_image : String,
	@SerializedName("commitment_4_image") val commitment_4_image : String,
	@SerializedName("commitment_5_image") val commitment_5_image : String
)