package com.commerce.antree.data.model.place

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/*
Copyright (c) 2020 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */


data class Merchant (

	@SerializedName("id") val id : Int,
	@SerializedName("user_id") val user_id : Int,
	@SerializedName("place_categories_id") val place_categories_id : Int,
	@SerializedName("name") val name : String,
	@SerializedName("availability") val availability : String,
	@SerializedName("health_verified") val health_verified : Int,
	@SerializedName("paket") val paket : String,
	@SerializedName("closed_reason") val closed_reason : String,
	@SerializedName("open_hour") val open_hour : String,
	@SerializedName("close_hour") val close_hour : String,
	@SerializedName("open_mon") val open_mon : Int,
	@SerializedName("open_tue") val open_tue : Int,
	@SerializedName("open_wed") val open_wed : Int,
	@SerializedName("open_thurs") val open_thurs : Int,
	@SerializedName("open_fri") val open_fri : Int,
	@SerializedName("open_sat") val open_sat : Int,
	@SerializedName("open_sun") val open_sun : Int,
	@SerializedName("long") val long : Double,
	@SerializedName("lat") val lat : Double,
	@SerializedName("address") val address : String,
	@SerializedName("postal_code") val postal_code : Int,
	@SerializedName("district") val district : String,
	@SerializedName("city") val city : String,
	@SerializedName("province") val province : String,
	@SerializedName("country") val country : String,
	@SerializedName("reservation_type") val reservation_type : String,
	@SerializedName("created_at") val created_at : String,
	@SerializedName("updated_at") val updated_at : String,
	@SerializedName("image_condition_1") val image_condition_1 : String,
	@SerializedName("image_condition_2") val image_condition_2 : String,
	@SerializedName("image_condition_3") val image_condition_3 : String,
	@SerializedName("image_condition_4") val image_condition_4 : String,
	@SerializedName("image_condition_5") val image_condition_5 : String,
	@SerializedName("queue_total") val queue_total : Int,
	@SerializedName("category") val category : Category,
	@SerializedName("image") val image : String,
	@SerializedName("gallery") val gallery : List<Gallery>,
	@SerializedName("health_protocol") val health_protocol : HealthProtocol,
	@SerializedName("staff_check_in") val staff_check_in : List<String>,
	@SerializedName("count_1") val count_1 : Int,
	@SerializedName("count_2") val count_2 : Int,
	@SerializedName("count_3") val count_3 : Int,
	@SerializedName("count_4") val count_4 : Int,
	@SerializedName("count_5") val count_5 : Int,
	@SerializedName("review_score") val review_score : Double,
	@SerializedName("star") val star : Int,
	@SerializedName("counter") val counter : List<Counter>
)