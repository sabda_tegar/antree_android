package com.commerce.antree.data.repository

import com.commerce.antree.data.api.ApiHelper

class MainRepository (private val apiHelper: ApiHelper) {
    suspend fun getPlaces() = apiHelper.getPlaces()
    suspend fun getPlaceByID(merchantId: String) = apiHelper.getPlaceByID(merchantId)
}