package com.commerce.antree.dialog

import android.app.Dialog
import android.content.Context
import android.view.ViewGroup
import com.commerce.antree.R
import kotlinx.android.synthetic.main.dialog_feedback_sent.*
import kotlinx.android.synthetic.main.dialog_queue_sent.*

class DialogFeedbackSent {
    public lateinit var dialogFeedbackSent: Dialog

    fun showDialog(context: Context) {
        dialogFeedbackSent = Dialog(context)
        dialogFeedbackSent.setContentView(R.layout.dialog_feedback_sent)

        val window = dialogFeedbackSent.getWindow()
        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialogFeedbackSent.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)

        dialogFeedbackSent.btn_dialogfeedbacksent_ok.setOnClickListener { hideDialog() }
        //window!!.getAttributes().windowAnimations = R.style.DialogAnimation

        dialogFeedbackSent.show()
    }

    fun hideDialog() {
        dialogFeedbackSent.dismiss()
    }
}