package com.commerce.antree.dialog

import com.commerce.antree.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.annotation.Nullable
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_give_feedback_to_merchant.*

class DialogGiveFeedbackToMerchant : BottomSheetDialogFragment() {


    fun newInstance(): DialogGiveFeedbackToMerchant? {
        return DialogGiveFeedbackToMerchant()
    }

    private lateinit var dialogPleaseUseMask: DialogFeedbackSent

    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        // get the views and attach the listener
        activity!!.getWindow().setBackgroundDrawableResource(android.R.color.transparent)

        val root: View = inflater.inflate(
            R.layout.dialog_give_feedback_to_merchant, container,
            false
        )
        val button = root.findViewById<Button>(R.id.btn_givefeedback_send)

        button.setOnClickListener {
            showDialogFeedbackSent()
        }

        return root
    }

    fun showDialogFeedbackSent() {
        dialogPleaseUseMask = DialogFeedbackSent()
        context?.let { dialogPleaseUseMask.showDialog(it) }
    }
}
