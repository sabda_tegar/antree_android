package com.commerce.antree.dialog

import android.app.Dialog
import android.content.Context
import android.view.ViewGroup
import com.commerce.antree.R
import kotlinx.android.synthetic.main.dialog_please_use_mask.*
import kotlinx.android.synthetic.main.dialog_queue_sent.*

class DialogPleaseUseMask {
    public lateinit var dialogPleaseUseMask: Dialog

    fun showDialog(context: Context) {
        dialogPleaseUseMask = Dialog(context)
        dialogPleaseUseMask.setContentView(R.layout.dialog_please_use_mask)

        val window = dialogPleaseUseMask.getWindow()
        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialogPleaseUseMask.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)

        dialogPleaseUseMask.btn_dialogpleaseusemask_ok.setOnClickListener { hideDialog() }
        //window!!.getAttributes().windowAnimations = R.style.DialogAnimation

        dialogPleaseUseMask.show()
    }

    fun hideDialog() {
        dialogPleaseUseMask.dismiss()
    }

}