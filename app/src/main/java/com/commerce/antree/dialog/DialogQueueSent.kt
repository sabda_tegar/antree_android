package com.commerce.antree.dialog

import android.view.ViewGroup
import android.app.Dialog
import android.content.Context
import android.content.Intent
import com.commerce.antree.HomeActivity
import com.commerce.antree.R
import kotlinx.android.synthetic.main.dialog_queue_sent.*

class DialogQueueSent {
        public lateinit var dialogQueueSent: Dialog

        fun showDialog(context: Context) {
            dialogQueueSent = Dialog(context)
            dialogQueueSent.setContentView(R.layout.dialog_queue_sent)

            val window = dialogQueueSent.getWindow()
            window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialogQueueSent.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)

            dialogQueueSent.btn_dialogqueuesent_ok.setOnClickListener { hideDialog(context) }
            //window!!.getAttributes().windowAnimations = R.style.DialogAnimation

            dialogQueueSent.show()
        }

        fun hideDialog(context: Context) {
            dialogQueueSent.dismiss()
            context.startActivity(
                Intent(context, HomeActivity::class.java).putExtra("legacy", "queue")
            )
        }


}