package com.commerce.antree.dialog

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.commerce.antree.R
import com.commerce.antree.VerifyPhoneNumberActivity
import kotlinx.android.synthetic.main.dialog_feedback_sent.*
import kotlinx.android.synthetic.main.dialog_welcome_google.*

class DialogWelcomeGoogle {
    public lateinit var dialogWelcomeGoogle: Dialog

    fun showDialog(context: Context,
    accountName: String, accountPicture: Uri?) {
        dialogWelcomeGoogle = Dialog(context)
        dialogWelcomeGoogle.setContentView(R.layout.dialog_welcome_google)

        val window = dialogWelcomeGoogle.getWindow()
        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialogWelcomeGoogle.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)

        dialogWelcomeGoogle.txt_dialogwelcomegoogle_accountname.text = accountName

        if (accountPicture != null) {
            Glide.with(context)
                .load(accountPicture)
                .transform(CircleCrop())
                .into(dialogWelcomeGoogle.img_dialogwelcomegoogle_accountpicture)
        }

        dialogWelcomeGoogle.btn_dialogwelcomegoogle_next.setOnClickListener { hideDialog(context) }
        //window!!.getAttributes().windowAnimations = R.style.DialogAnimation

        dialogWelcomeGoogle.show()
    }

    fun hideDialog(context: Context) {
        context.startActivity(
            Intent(context, VerifyPhoneNumberActivity::class.java)
        )
    }
}