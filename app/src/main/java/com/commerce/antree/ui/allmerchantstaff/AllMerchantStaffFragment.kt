package com.commerce.antree.ui.allmerchantstaff

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.commerce.antree.R
import com.commerce.antree.ui.merchantdetail.adapter.MerchantStaffAdapter
import com.commerce.antree.ui.merchantdetail.data.MerchantStaff
import kotlinx.android.synthetic.main.all_merchant_staff_fragment.*
import kotlinx.android.synthetic.main.book_merchant_fragment.*
import kotlinx.android.synthetic.main.merchant_detail_fragment.*

class AllMerchantStaffFragment : Fragment() {

    companion object {
        fun newInstance() = AllMerchantStaffFragment()
    }

    private lateinit var viewModel: AllMerchantStaffViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.all_merchant_staff_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AllMerchantStaffViewModel::class.java)
        setToolbar()
        // TODO: Use the ViewModel
        val merchantStaff: List<MerchantStaff> = listOf(
            MerchantStaff("1", R.drawable.profile_tegar, "Sabda Tegar", "Chef"),
            MerchantStaff("2", R.drawable.profile_anti, "Anti", "Pramusaji"),
            MerchantStaff("3", R.drawable.profile_ijal, "Izal", "Kasir")
        )

        val merchantStaffAdapter = MerchantStaffAdapter(merchantStaff)
        allmerchantstaff_recyclerview_allstaff.setLayoutManager(GridLayoutManager(activity, 3))
        allmerchantstaff_recyclerview_allstaff.adapter = merchantStaffAdapter
    }

    private fun setToolbar() {

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(toolbar_allmerchantstaff)
            (activity as AppCompatActivity).supportActionBar?.title= ""
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }
}