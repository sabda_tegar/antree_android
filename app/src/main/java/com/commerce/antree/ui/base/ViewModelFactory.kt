package com.commerce.antree.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.commerce.antree.data.api.ApiHelper
import com.commerce.antree.data.repository.MainRepository
import com.commerce.antree.ui.home.HomeViewModel
import com.commerce.antree.ui.main.LoginViewModel
import com.commerce.antree.ui.merchantdetail.MerchantDetailViewModel

class ViewModelFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(MainRepository(apiHelper)) as T
        } else if (modelClass.isAssignableFrom(MerchantDetailViewModel::class.java)) {
            return MerchantDetailViewModel(MainRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}