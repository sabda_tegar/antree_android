package com.commerce.antree.ui.chooseinterest

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Typeface
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.commerce.antree.HomeActivity
import com.commerce.antree.R
import com.commerce.antree.SetMyPlacesActivity
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.choose_interest_fragment.*

class ChooseInterestFragment : Fragment() {

    companion object {
        fun newInstance() = ChooseInterestFragment()
    }

    private lateinit var viewModel: ChooseInterestViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.choose_interest_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChooseInterestViewModel::class.java)
        // TODO: Use the ViewModel
        btn_chooseinterest_next.setOnClickListener {
            startActivity(Intent(activity, SetMyPlacesActivity::class.java))
        }

        addChipToGroup("FINE DINING", chipGroup)
        addChipToGroup("CAFE", chipGroup)
        addChipToGroup("CLINIC", chipGroup)
        addChipToGroup("BANK", chipGroup)
        addChipToGroup("FOOD COURT", chipGroup)
        addChipToGroup("GARDEN", chipGroup)
        addChipToGroup("BIKE SHOP", chipGroup)
        addChipToGroup("KANTOR CAMAT", chipGroup)
        addChipToGroup("DENTIST", chipGroup)
        addChipToGroup("CAR SHOP", chipGroup)
        addChipToGroup("PARK", chipGroup)
        addChipToGroup("FASTFOOD", chipGroup)
        addChipToGroup("COFFEE SHOP", chipGroup)
        addChipToGroup("ALL YOU CAN EAT", chipGroup)
        addChipToGroup("INSURANCE", chipGroup)
    }

    @SuppressLint("NewApi")
    private fun addChipToGroup(person: String, chipGroup: ChipGroup) {
        val chip = Chip(chipGroup.context)

        chip.text = person
        chip.chipCornerRadius = maxOf(50f, 50f)
        chip.checkedIcon = null
        chip.setTextColor(resources.getColor(R.color.colorTextGray))
        chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(android.R.color.white))
        //chip.chipIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_vw_circle_green)
        chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorTextGray))
        chip.chipStrokeWidth = maxOf(1f, 1f)
        // necessary to get single selection working
        chip.isClickable = true
        chip.isCheckable = true
        chip.apply {
            setTextAppearance(R.style.chipTextappearance)
        }
        chipGroup.addView(chip as View)
        chip.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                chip.setTextColor(resources.getColor(android.R.color.white))
                chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(R.color.colorAccent))
                chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorAccent))
            } else {
                chip.setTextColor(resources.getColor(R.color.colorTextGray))
                chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(android.R.color.white))
                chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorTextGray))
            }
        }
    }
}
