package com.commerce.antree.ui.givefeedbacktomerchant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.commerce.antree.HomeActivity
import com.commerce.antree.R
import kotlinx.android.synthetic.main.give_feedback_to_merchant_fragment.*


class GiveFeedbackToMerchantFragment : Fragment() {

    companion object {
        fun newInstance() = GiveFeedbackToMerchantFragment()
    }

    private lateinit var viewModel: GiveFeedbackToMerchantViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.give_feedback_to_merchant_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GiveFeedbackToMerchantViewModel::class.java)
        // TODO: Use the ViewModel
        switch_givefeedback_one.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                switch_givefeedback_one.setThumbDrawable(
                    activity!!.resources.getDrawable(R.drawable.givefeedback_iconthumbsup)
                )
                txt_givefeedback_oneyes.setTextColor(resources.getColor(R.color.colorAccent))
                txt_givefeedback_oneno.setTextColor(resources.getColor(R.color.colorTextGray))
            } else {
                switch_givefeedback_one.setThumbDrawable(activity!!.resources.getDrawable(R.drawable.givefeedback_thumbsdown))
                txt_givefeedback_oneno.setTextColor(resources.getColor(android.R.color.holo_red_light))
                txt_givefeedback_oneyes.setTextColor(resources.getColor(R.color.colorTextGray))
            }
        }

        switch_givefeedback_two.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                switch_givefeedback_two.setThumbDrawable(
                    activity!!.resources.getDrawable(R.drawable.givefeedback_iconthumbsup)
                )
                txt_givefeedback_twoyes.setTextColor(resources.getColor(R.color.colorAccent))
                txt_givefeedback_twono.setTextColor(resources.getColor(R.color.colorTextGray))
            } else {
                switch_givefeedback_two.setThumbDrawable(activity!!.resources.getDrawable(R.drawable.givefeedback_thumbsdown))
                txt_givefeedback_twono.setTextColor(resources.getColor(android.R.color.holo_red_light))
                txt_givefeedback_twoyes.setTextColor(resources.getColor(R.color.colorTextGray))
            }
        }

        switch_givefeedback_three.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                switch_givefeedback_three.setThumbDrawable(
                    activity!!.resources.getDrawable(R.drawable.givefeedback_iconthumbsup)
                )
                txt_givefeedback_threeyes.setTextColor(resources.getColor(R.color.colorAccent))
                txt_givefeedback_threeno.setTextColor(resources.getColor(R.color.colorTextGray))
            } else {
                switch_givefeedback_three.setThumbDrawable(activity!!.resources.getDrawable(R.drawable.givefeedback_thumbsdown))
                txt_givefeedback_threeno.setTextColor(resources.getColor(android.R.color.holo_red_light))
                txt_givefeedback_threeyes.setTextColor(resources.getColor(R.color.colorTextGray))
            }
        }

        switch_givefeedback_four.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                switch_givefeedback_four.setThumbDrawable(
                    activity!!.resources.getDrawable(R.drawable.givefeedback_iconthumbsup)
                )
                txt_givefeedback_fouryes.setTextColor(resources.getColor(R.color.colorAccent))
                txt_givefeedback_fourno.setTextColor(resources.getColor(R.color.colorTextGray))
            } else {
                switch_givefeedback_four.setThumbDrawable(activity!!.resources.getDrawable(R.drawable.givefeedback_thumbsdown))
                txt_givefeedback_fourno.setTextColor(resources.getColor(android.R.color.holo_red_light))
                txt_givefeedback_fouryes.setTextColor(resources.getColor(R.color.colorTextGray))
            }
        }

        switch_givefeedback_five.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                switch_givefeedback_five.setThumbDrawable(
                    activity!!.resources.getDrawable(R.drawable.givefeedback_iconthumbsup)
                )
                txt_givefeedback_fiveyes.setTextColor(resources.getColor(R.color.colorAccent))
                txt_givefeedback_fiveno.setTextColor(resources.getColor(R.color.colorTextGray))
            } else {
                switch_givefeedback_five.setThumbDrawable(activity!!.resources.getDrawable(R.drawable.givefeedback_thumbsdown))
                txt_givefeedback_fiveno.setTextColor(resources.getColor(android.R.color.holo_red_light))
                txt_givefeedback_fiveyes.setTextColor(resources.getColor(R.color.colorTextGray))
            }
        }
    }

}