package com.commerce.antree.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.commerce.antree.HomeActivity
import com.commerce.antree.MerchantActivity
import com.commerce.antree.R
import com.commerce.antree.data.api.ApiHelper
import com.commerce.antree.data.api.RetrofitBuilder
import com.commerce.antree.data.model.place.Merchant
import com.commerce.antree.dialog.DialogPleaseUseMask
import com.commerce.antree.ui.base.ViewModelFactory
import com.commerce.antree.ui.home.adapter.MerchantCategoryAdapter
import com.commerce.antree.ui.home.adapter.MerchantPlaceAdapter
import com.commerce.antree.ui.home.data.MerchantCategory
import com.commerce.antree.util.Status

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    lateinit var recyclerViewMerchantPlace: RecyclerView
    lateinit var merchantPlaceAdapter: MerchantPlaceAdapter

    private lateinit var dialogPleaseUseMask: DialogPleaseUseMask
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setupViewModel()
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        val recyclerViewMerchantCategory: RecyclerView = root.findViewById(R.id.home_recyclerview_merchantcategory)
        recyclerViewMerchantPlace = root.findViewById(R.id.home_recyclerview_merchantplace)

        setupUI()
        setupObservers()

        /*homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })*/

        val listMerchantCategory = listOf(
            MerchantCategory(name = "Restaurant", icon = R.drawable.home_iconrestaurant_white, category = 1),
            MerchantCategory(name = "Cafe", icon = R.drawable.home_iconcafe_white, category = 1),
            MerchantCategory(name = "Fast Food", icon = R.drawable.home_iconfastfoodwhite, category = 1),
            MerchantCategory(name = "Bank", icon = R.drawable.home_iconbank_white, category = 3),
            MerchantCategory(name = "Bike Shop", icon = R.drawable.home_iconbike_white, category = 4),
            MerchantCategory(name = "Clinic", icon = R.drawable.home_iconclinic_white, category = 2),
            MerchantCategory(name = "Hospital", icon = R.drawable.home_iconhospital_white, category = 2),
            MerchantCategory(name = "Dentist", icon = R.drawable.home_icondentist_white, category = 2),
            MerchantCategory(name = "CU", icon = R.drawable.home_iconcoffeeshop_white, category = 3),
            MerchantCategory(name = "See All", icon = R.drawable.home_iconallmenu_white, category = 0)
        )

        val merchantCategoryAdapter = MerchantCategoryAdapter(listMerchantCategory)

        recyclerViewMerchantCategory.setLayoutManager(GridLayoutManager(activity, 5))
        recyclerViewMerchantCategory.adapter = merchantCategoryAdapter
        recyclerViewMerchantCategory.setOnClickListener {
            val intent = Intent(activity, MerchantActivity::class.java)
            startActivity(intent)
        }

       /* val listMerchantPlaces = listOf(
            MerchantPlace(name = "Ichiban Sushi", icon = R.drawable.merchant_ichiban, address = "Kompleks Mega Mall, Pontianak"),
            MerchantPlace(name = "Solaria", icon = R.drawable.solaria, address = "Sungai Raya Dalam, Pontianak"),
            MerchantPlace(name = "Dinasty", icon = R.drawable.dinasty, address = "Sungai Raya Dalam, Pontianak"),
            MerchantPlace(name = "Mandiri Kubu", icon = R.drawable.mandiri_kubu, address = "Sungai Raya Dalam, Pontianak"),
            MerchantPlace(name = "Dinasty", icon = R.drawable.dinasty, address = "Sungai Raya Dalam, Pontianak"),
            MerchantPlace(name = "Solaria", icon = R.drawable.solaria, address = "Sungai Raya Dalam, Pontianak")
        )

        val merchantPlaceAdapter = MerchantPlaceAdapter(listMerchantPlaces)

        recyclerViewMerchantPlace.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false))
        recyclerViewMerchantPlace.adapter = merchantPlaceAdapter*/

        //showDialogQueueSent()

        return root
    }

    fun showDialogQueueSent() {
        dialogPleaseUseMask = DialogPleaseUseMask()
        context?.let { dialogPleaseUseMask.showDialog(it) }
    }

    private fun setupViewModel() {
        homeViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(HomeViewModel::class.java)
    }

    private fun setupUI() {
        recyclerViewMerchantPlace.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        merchantPlaceAdapter = MerchantPlaceAdapter(arrayListOf())
        recyclerViewMerchantPlace.adapter = merchantPlaceAdapter
    }

    private fun setupObservers() {
        homeViewModel.getPlaces().observe(activity!!, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        //recyclerView.visibility = View.VISIBLE
                        //progressBar.visibility = View.GONE
                        resource.data?.let { places -> retrieveList(places.merchant) }
                    }
                    Status.ERROR -> {
                        //recyclerView.visibility = View.VISIBLE
                        //progressBar.visibility = View.GONE
                        Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                        Log.d(HomeActivity::class.java.simpleName, it.message)
                    }
                    Status.LOADING -> {
                        //progressBar.visibility = View.VISIBLE
                        //recyclerView.visibility = View.GONE
                        Log.d(HomeActivity::class.java.simpleName, "loading")
                    }
                }
            }
        })
    }

    private fun retrieveList(places: List<Merchant>) {
        merchantPlaceAdapter.apply {
            addMerchant(places)
            notifyDataSetChanged()
        }
    }
}