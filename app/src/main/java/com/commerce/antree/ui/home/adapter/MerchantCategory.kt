package com.commerce.antree.ui.home.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.commerce.antree.MerchantActivity
import com.commerce.antree.MerchantDetailActivity
import com.commerce.antree.ui.home.data.MerchantCategory

import com.commerce.antree.R
import kotlinx.android.synthetic.main.item_merchant_category.view.*

class MerchantCategoryAdapter(private val merchantCategory: List<MerchantCategory>) : RecyclerView.Adapter<MerchantCategoryHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MerchantCategoryHolder {
        return MerchantCategoryHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_merchant_category, viewGroup, false))
    }

    override fun getItemCount(): Int = merchantCategory.size

    override fun onBindViewHolder(holder: MerchantCategoryHolder, position: Int) {
        holder.bindHero(merchantCategory[position])
    }
}


class MerchantCategoryHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvMerchantName = view.txt_merchant_name
    private val imgMerchantIcon = view.img_merchant_icon
    private val merchantBackground = view.viewgroup_merchantbackground

    fun bindHero(merchantCategory: MerchantCategory) {
        tvMerchantName.text = merchantCategory.name
        itemView?.let {
            Glide.with(it)
                .load(merchantCategory.icon)
                .into(imgMerchantIcon)
        }
        itemView?.let {
            when (merchantCategory.category) {
                1 -> merchantBackground.setBackgroundColor( it.resources.getColor(R.color.colorAccent))
                2 -> merchantBackground.setBackgroundColor( it.resources.getColor(R.color.colorOrange))
                3 -> merchantBackground.setBackgroundColor( it.resources.getColor(R.color.colorPurple))
                4 -> merchantBackground.setBackgroundColor( it.resources.getColor(R.color.colorAqua))
                else -> merchantBackground.setBackgroundColor( it.resources.getColor(android.R.color.transparent))
            }
        }

        itemView.setOnClickListener {
            val intent = Intent(it.context, MerchantActivity::class.java)
            it.context.startActivity(intent)
        }
    }
}