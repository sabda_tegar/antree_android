package com.commerce.antree.ui.home.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.commerce.antree.MerchantDetailActivity
import com.commerce.antree.R
import com.commerce.antree.data.model.place.Merchant
import kotlinx.android.synthetic.main.item_merchant_place.view.*

class MerchantPlaceAdapter(private val merchantPlace: ArrayList<Merchant> ) : RecyclerView.Adapter<MerchantPlaceHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MerchantPlaceHolder {
        return MerchantPlaceHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_merchant_place, viewGroup, false))
    }

    override fun getItemCount(): Int = merchantPlace.size

    override fun onBindViewHolder(holder: MerchantPlaceHolder, position: Int) {
        holder.bindHero(merchantPlace[position])
    }

    fun addMerchant(merchantPlace: List<Merchant>) {
        this.merchantPlace.apply {
            clear()
            addAll(merchantPlace)
        }
    }
}

class MerchantPlaceHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvMerchantName = view.txt_merchantplace_name
    private val tvMerchantAddress = view.txt_merchantplace_address
    private val imgMerchantPlacePicture = view.img_merchantplace_picture

    fun bindHero(merchantPlace: Merchant) {
        tvMerchantName.text = merchantPlace.name
        tvMerchantAddress.text = merchantPlace.address
        itemView?.let {
            Glide.with(it)
                .load(merchantPlace.image)
                .into(imgMerchantPlacePicture)
        }

        itemView.setOnClickListener {
            val intent = Intent(it.context, MerchantDetailActivity::class.java)
            intent.putExtra("booking", "no")
            it.context.startActivity(intent)
        }
    }
}