package com.commerce.antree.ui.home.data

data class MerchantCategory (var name: String, var icon: Int, var category: Int)