package com.commerce.antree.ui.main

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.commerce.antree.ChooseInterestActivity
import com.commerce.antree.HomeActivity
import com.commerce.antree.R
import com.commerce.antree.VerifyPhoneNumberActivity
import com.commerce.antree.appintro.AppIntroActivity
import com.commerce.antree.dialog.DialogPleaseUseMask
import com.commerce.antree.dialog.DialogWelcomeGoogle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.login_fragment.*
import kotlin.math.sign

class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
        private const val TAG = "GoogleActivity"
        private const val RC_SIGN_IN = 9001
    }

    private lateinit var viewModel: LoginViewModel

    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]

    lateinit var rootView: View
    //private lateinit var binding: ActivityGoogleBinding
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var dialogWelcomeGoogle: DialogWelcomeGoogle

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        rootView = inflater.inflate(R.layout.login_fragment, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        // TODO: Use the ViewModel

        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(activity!!, gso)
        // Initialize Firebase Auth
        auth = Firebase.auth

        btn_login_signin.setOnClickListener {
            val intent = Intent(activity, ChooseInterestActivity::class.java)
            startActivity(intent)
        }

        btn_login_signup.setOnClickListener {
            revokeAccess()
        }

        btn_login_signinwithgoogle.setOnClickListener {
            signIn()
        }
    }

    // [START on_start_check_user]
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    // [START auth_with_google]
    private fun firebaseAuthWithGoogle(idToken: String) {
        // [END_EXCLUDE]
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                Log.d(TAG, "signInWithCredential:success")
                val user = auth.currentUser
                updateUI(user)
            } else {
                // If sign in fails, display a message to the user.
                Log.w(TAG, "signInWithCredential:failure", it.exception)
                // [END_EXCLUDE]
                Snackbar.make(rootView, "Authentication Failed.", Snackbar.LENGTH_SHORT).show()
                updateUI(null)
            }
        }
    }
    // [END auth_with_google]

    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
                // ...
            }
        }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            Log.d(TAG, user.uid)
            //Log.d(TAG, user.phoneNumber)
            Log.d(TAG, user.email)
            Log.d(TAG, user.displayName)
            var photo: Uri? = null
            photo = user.photoUrl

            if (photo != null) {
                showDialogWelcomeGoogle(user.displayName.toString(), photo)
            } else {
                showDialogWelcomeGoogle(user.displayName.toString(), null)
            }
        } else {
            Log.d(TAG, "DATA NULL")
        }
    }

    private fun revokeAccess() {
        // Firebase sign out
        auth.signOut()

        // Google revoke access
        googleSignInClient.revokeAccess().addOnCompleteListener{
            updateUI(null)
        }
    }

    fun showDialogWelcomeGoogle(accountName: String, accountPicture: Uri?) {
        dialogWelcomeGoogle = DialogWelcomeGoogle()
        context?.let { dialogWelcomeGoogle.showDialog(it, accountName, accountPicture) }
    }
}