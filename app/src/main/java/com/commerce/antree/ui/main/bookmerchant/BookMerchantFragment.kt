package com.commerce.antree.ui.main.bookmerchant

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.commerce.antree.R
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.book_merchant_fragment.*
import kotlinx.android.synthetic.main.choose_interest_fragment.*
import kotlinx.android.synthetic.main.queue_merchant_fragment.*

class BookMerchantFragment : Fragment() {

    companion object {
        fun newInstance() = BookMerchantFragment()
    }

    private lateinit var viewModel: BookMerchantViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.book_merchant_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BookMerchantViewModel::class.java)
        setToolbar()
        // TODO: Use the ViewModel


        addChipToGroup("SENIN", chipgroup_bookmerchant_bookingday)
        addChipToGroup("SELASA", chipgroup_bookmerchant_bookingday)
        addChipToGroup("RABU", chipgroup_bookmerchant_bookingday)
        addChipToGroup("KAMIS", chipgroup_bookmerchant_bookingday)
        addChipToGroup("JUM'AT", chipgroup_bookmerchant_bookingday)
        addChipToGroup("SABTU", chipgroup_bookmerchant_bookingday)
        addChipToGroup("MINGGU", chipgroup_bookmerchant_bookingday)

        addChipToGroup("PAGI", chipgroup_bookmerchant_bookingshift)
        addChipToGroup("SIANG", chipgroup_bookmerchant_bookingshift)
        addChipToGroup("MALAM", chipgroup_bookmerchant_bookingshift)

        addChipToGroup("11.30", chipgroup_bookmerchant_bookingtime)
        addChipToGroup("12.30", chipgroup_bookmerchant_bookingtime)
        addChipToGroup("13.30", chipgroup_bookmerchant_bookingtime)
        addChipToGroup("14.30", chipgroup_bookmerchant_bookingtime)
    }

    @SuppressLint("NewApi")
    private fun addChipToGroup(person: String, chipGroup: ChipGroup) {
        val chip = Chip(chipGroup.context)

        chip.text = person
        chip.chipCornerRadius = maxOf(20f, 20f)
        chip.checkedIcon = null
        chip.setTextColor(resources.getColor(R.color.colorTextGray))
        chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(android.R.color.white))
        //chip.chipIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_vw_circle_green)
        chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorTextGray))
        chip.chipStrokeWidth = maxOf(1f, 1f)
        // necessary to get single selection working
        chip.isClickable = true
        chip.isCheckable = true
        chip.apply {
            setTextAppearance(R.style.chipTextappearance)
        }
        chipGroup.addView(chip as View)
        chip.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                chip.setTextColor(resources.getColor(android.R.color.white))
                chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(R.color.colorAccent))
                chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorAccent))

                when (compoundButton.text.toString().toLowerCase()) {
                    "senin" -> txt_bookmerchant_bookingday.text = "Senin, 31 Agustus 2020"
                    "selasa" -> txt_bookmerchant_bookingday.text = "Selasa, 1 September 2020"
                    "rabu" -> txt_bookmerchant_bookingday.text = "Rabu, 2 September 2020"
                    "kamis" -> txt_bookmerchant_bookingday.text = "Kamis, 3 September 2020"
                    "jum'at" -> txt_bookmerchant_bookingday.text = "Jum'at, 4 September 2020"
                    "sabtu" -> txt_bookmerchant_bookingday.text = "Sabtu, 5 September 2020"
                    "minggu" -> txt_bookmerchant_bookingday.text = "Minggu, 6 September 2020"
                }
            } else {
                chip.setTextColor(resources.getColor(R.color.colorTextGray))
                chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(android.R.color.white))
                chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorTextGray))
            }
        }
    }

    private fun setToolbar() {

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(toolbar_bookmerchant)
            (activity as AppCompatActivity).supportActionBar?.title= ""
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }
}