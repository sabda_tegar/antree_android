package com.commerce.antree.ui.main.merchant

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.commerce.antree.R
import com.commerce.antree.ui.main.merchant.adapter.MerchantAdapter
import com.commerce.antree.ui.mybooking.adapter.MyBookingAdapter
import com.commerce.antree.ui.mybooking.data.MyBooking
import kotlinx.android.synthetic.main.book_merchant_fragment.*
import kotlinx.android.synthetic.main.merchant_fragment.*

class MerchantFragment : Fragment() {

    companion object {
        fun newInstance() = MerchantFragment()
    }

    private lateinit var viewModel: MerchantViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.merchant_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MerchantViewModel::class.java)
        setToolbar()
        // TODO: Use the ViewModel
        val merchants: List<MyBooking> = listOf(
            MyBooking("1", R.drawable.merchant_ichiban, "Ichiban Sushi", "Restaurant"),
            MyBooking("2", R.drawable.solaria, "Solaria", "Restaurant"),
            MyBooking("3", R.drawable.dinasty, "Dinasty", "Restaurant")
        )

        val merchantAdapter = MerchantAdapter(merchants)
        recyclerview_merchant.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false))
        recyclerview_merchant.adapter = merchantAdapter
    }

    private fun setToolbar() {

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(toolbar_merchant)
            (activity as AppCompatActivity).supportActionBar?.title= ""
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }
}