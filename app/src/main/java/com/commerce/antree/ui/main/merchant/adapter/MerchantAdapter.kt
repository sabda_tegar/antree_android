package com.commerce.antree.ui.main.merchant.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.commerce.antree.MerchantDetailActivity
import com.commerce.antree.R
import com.commerce.antree.ui.mybooking.data.MyBooking
import kotlinx.android.synthetic.main.item_merchant.view.*

class MerchantAdapter(private val myBooking: List<MyBooking> ) : RecyclerView.Adapter<MerchantHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MerchantHolder {
        return MerchantHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_merchant, viewGroup, false))
    }

    override fun getItemCount(): Int = myBooking.size

    override fun onBindViewHolder(holder: MerchantHolder, position: Int) {
        holder.bindHero(myBooking[position])
    }
}


class MerchantHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvMerchantName = view.txt_merchant_name
    private val tvMerchantAddress = view.txt_merchant_category
    private val imgMerchantPhoto = view.img_merchant_photo
    private val tvMerchantQueue = view.txt_merchant_queue

    fun bindHero(myBooking: MyBooking) {
        tvMerchantName.text = myBooking.merchantName
        tvMerchantAddress.text = myBooking.merchantCategory
        itemView?.let {
            Glide.with(it)
                .load(myBooking.merchantPhoto)
                .into(imgMerchantPhoto)
        }
        when(myBooking.id){
            "1" -> tvMerchantQueue.text = "36"
            "2" -> tvMerchantQueue.text = "30"
            "3" -> tvMerchantQueue.text = "33"
        }
        itemView.setOnClickListener {
            val intent = Intent(it.context, MerchantDetailActivity::class.java)
            intent.putExtra("booking", "no")
            intent.putExtra("status", 0)
            it.context.startActivity(intent)
        }
    }
}
