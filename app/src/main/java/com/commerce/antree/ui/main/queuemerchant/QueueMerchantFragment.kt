package com.commerce.antree.ui.main.queuemerchant

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.res.ColorStateList
import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.commerce.antree.R
import com.commerce.antree.dialog.DialogQueueSent
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.book_merchant_fragment.*
import kotlinx.android.synthetic.main.queue_merchant_fragment.*

class QueueMerchantFragment : Fragment() {

    companion object {
        fun newInstance() = QueueMerchantFragment()
    }

    private lateinit var viewModel: QueueMerchantViewModel

    private lateinit var dialogQueueSent: DialogQueueSent

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.queue_merchant_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(QueueMerchantViewModel::class.java)
        setToolbar()
        // TODO: Use the ViewModel
        addChipToGroup("Utama", chipgroup_queuemerchant_service)
        addChipToGroup("Ojek Online", chipgroup_queuemerchant_service)

        btn_merchantqueue_register.setOnClickListener {
            showDialogQueueSent()
        }
        //startChronometer()
    }

    @SuppressLint("NewApi")
    private fun addChipToGroup(person: String, chipGroup: ChipGroup) {
        val chip = Chip(chipGroup.context)

        chip.text = person
        chip.chipCornerRadius = maxOf(20f, 20f)
        chip.checkedIcon = null
        chip.setTextColor(resources.getColor(R.color.colorTextGray))
        chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(android.R.color.white))
        //chip.chipIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_vw_circle_green)
        chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorTextGray))
        chip.chipStrokeWidth = maxOf(1f, 1f)
        // necessary to get single selection working
        chip.isClickable = true
        chip.isCheckable = true
        chip.apply {
            setTextAppearance(R.style.chipTextappearance)
        }
        chipGroup.addView(chip as View)
        chip.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                chip.setTextColor(resources.getColor(android.R.color.white))
                chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(R.color.colorAccent))
                chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorAccent))

                Log.d("compound", compoundButton.text.toString())

                when (compoundButton.text.toString().toLowerCase()) {
                    "utama" -> {
                        txt_queuemerchant_service.text = "Jumlah antrian : 15"
                        txt_queuemerchant_estimationtime.text = "10:35:00"
                    }
                    "ojek online" -> {
                        txt_queuemerchant_service.text = "Jumlah antrian : 21"
                        txt_queuemerchant_estimationtime.text = "11:15:00"
                    }
                }
            } else {
                chip.setTextColor(resources.getColor(R.color.colorTextGray))
                chip.chipBackgroundColor = ColorStateList.valueOf(resources.getColor(android.R.color.white))
                chip.chipStrokeColor = ColorStateList.valueOf(resources.getColor(R.color.colorTextGray))
            }
        }
    }

    fun showDialogQueueSent() {
        dialogQueueSent = DialogQueueSent()
        context?.let { dialogQueueSent.showDialog(it) }
    }

//    @TargetApi(Build.VERSION_CODES.N)
//    private fun startChronometer(){
//        chronometer_queuemerchant_callingtime.setBase(SystemClock.elapsedRealtime())
//        chronometer_queuemerchant_callingtime.start()
//    }
//
//    @RequiresApi(Build.VERSION_CODES.N)
//    private fun stopChronometer(){
//        chronometer_queuemerchant_callingtime.isCountDown = false
//        chronometer_queuemerchant_callingtime.stop()
//        chronometer_queuemerchant_callingtime.text = "00:00"
//    }

    private fun setToolbar() {

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(toolbar_queuemerchant)
            (activity as AppCompatActivity).supportActionBar?.title= ""
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

}