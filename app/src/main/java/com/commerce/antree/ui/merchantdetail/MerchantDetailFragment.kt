package com.commerce.antree.ui.merchantdetail

import android.content.Intent
import android.graphics.Color
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.commerce.antree.*
import com.commerce.antree.data.api.ApiHelper
import com.commerce.antree.data.api.RetrofitBuilder
import com.commerce.antree.data.model.place.Merchant
import com.commerce.antree.dialog.DialogGiveFeedbackToMerchant
import com.commerce.antree.ui.base.ViewModelFactory
import com.commerce.antree.ui.home.HomeViewModel
import com.commerce.antree.ui.home.adapter.MerchantPlaceAdapter
import com.commerce.antree.ui.home.data.MerchantPlace
import com.commerce.antree.ui.merchantdetail.MerchantDetailFragment.Companion.newInstance
import com.commerce.antree.ui.merchantdetail.adapter.HealthyProtocolAdapter
import com.commerce.antree.ui.merchantdetail.adapter.MerchantStaffAdapter
import com.commerce.antree.ui.merchantdetail.data.HealthyProtocol
import com.commerce.antree.ui.merchantdetail.data.MerchantStaff
import com.commerce.antree.ui.merchantgallery.data.MerchantGallery
import com.commerce.antree.util.Status
import kotlinx.android.synthetic.main.merchant_detail_fragment.*

class MerchantDetailFragment : Fragment() {

    companion object {
        fun newInstance() = MerchantDetailFragment()
    }

    private lateinit var viewModel: MerchantDetailViewModel

    var booking: String = "no"
    var status: Int = 0
    var merchant: Merchant? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.merchant_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
        // TODO: Use the ViewModel
        viewpager_merchantdetail_photoplace.adapter = MerchantDetailImageSliderAdapter(activity?.supportFragmentManager)
        indicator_merchantdetail_slider.setViewPager(viewpager_merchantdetail_photoplace)

        booking = activity!!.intent.getStringExtra("booking")
        status = activity!!.intent.getIntExtra("status", status)

        setupUIOffline()

        activity!!.window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
        //dialogTrashbankPointFragment.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(MerchantDetailViewModel::class.java)
    }

    private fun setupUIOffline() {
        if (booking.equals("yes")) {
            viewgroup_merchantdetail_ticket.visibility = View.VISIBLE
            txt_merchantdetail_ticket.text = "Terdaftar"
            btn_merchantdetail_toqueue.setBackground(resources.getDrawable(R.drawable.rectangle_gray))

            if (status == 5) {
                val dialogTrashbankPointFragment = DialogGiveFeedbackToMerchant().newInstance()
                dialogTrashbankPointFragment!!.show(activity!!.getSupportFragmentManager(),
                    "Dialog Trashbank Point")
            }

        } else {
            viewgroup_merchantdetail_ticket.visibility = View.GONE
            txt_merchantdetail_ticket.text = "Daftar"
            btn_merchantdetail_toqueue.setBackground(resources.getDrawable(R.drawable.button_main))
        }

        val healthyProtocols = listOf(
            HealthyProtocol(doing = true, desc = "Penerapan sirkulasi, batas waktu kunjungan, jumlah kunjungan, dan penerapan kontrol pada pintu masuk dan keluar."),
            HealthyProtocol(doing = true, desc = "Petugas dan pengunjung wajib menggunakan masker / face shield, menjaga jarak, dan kontrol suhu tubuh dibawah 37,5 C."),
            HealthyProtocol(doing = true, desc = "Pengelola lokasi selalu menjaga kebersihan dan menyemprotkan desinfektan pada lokasi secara berkala."),
            HealthyProtocol(doing = true, desc = "Batas jarak antrian saat pembelian / transaksi di kasir dalam rentang 1 meter dan maksimal 5 orang."),
            HealthyProtocol(doing = true, desc = "Pengelola menyediakan tempat cuci tangan / hand sanitizer.")
        )

        val healthyProtocolAdapter = HealthyProtocolAdapter(healthyProtocols)
        recyclerview_merchantdetail_healthyprotocol.setLayoutManager(LinearLayoutManager(activity))
        recyclerview_merchantdetail_healthyprotocol.adapter = healthyProtocolAdapter

        val merchantStaff: List<MerchantStaff> = listOf(
            MerchantStaff("1", R.drawable.profile_tegar, "Sabda Tegar", "Chef"),
            MerchantStaff("2", R.drawable.profile_anti, "Anti", "Pramusaji"),
            MerchantStaff("3", R.drawable.profile_ijal, "Izal", "Kasir"),
            MerchantStaff("4", R.drawable.profile_tegar, "Tegar", "Pramusaji"),
            MerchantStaff("5", R.drawable.profile_anti, "Anti", "Chef")
        )

        val merchantStaffAdapter = MerchantStaffAdapter(merchantStaff)
        recyclerview_merchantdetail_merchantstaff.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false))
        recyclerview_merchantdetail_merchantstaff.adapter = merchantStaffAdapter

        btn_merchantdetail_toqueue.setOnClickListener {
            if (booking.equals("no")) {
                val intent = Intent(activity, QueueMerchantActivity::class.java)
                startActivity(intent)
            }
        }

        btn_merchantdetail_todirection.setOnClickListener {
            val intent = Intent(activity, BookMerchantActivity::class.java)
            startActivity(intent)
        }

        btn_merchantdetail_toreview.setOnClickListener {
            val intent = Intent(activity, MerchantReviewedActivity::class.java)
            startActivity(intent)
        }

        txt_merchantdetail_toallstaff.setOnClickListener {
            val intent = Intent(activity, AllMerchantStaffActivity::class.java)
            startActivity(intent)
        }

        viewgroup_merchantdetail_togallery.setOnClickListener {
            val intent = Intent(activity, MerchantGalleryActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setupUIOnline() {
        setupObservers("1")
    }

    private fun setupObservers(merchantId: String) {
        viewModel.getPlaceByID(merchantId).observe(activity!!, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        //recyclerView.visibility = View.VISIBLE
                        //progressBar.visibility = View.GONE
                        resource.data?.let { merchant -> txt_merchantdetail_merchantname.text = merchant.name }
                    }
                    Status.ERROR -> {
                        //recyclerView.visibility = View.VISIBLE
                        //progressBar.visibility = View.GONE
                        Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                        Log.d(MerchantDetailActivity::class.java.simpleName, it.message)
                    }
                    Status.LOADING -> {
                        //progressBar.visibility = View.VISIBLE
                        //recyclerView.visibility = View.GONE
                        Log.d(MerchantDetailActivity::class.java.simpleName, "loading")
                    }
                }
            }
        })
    }
}