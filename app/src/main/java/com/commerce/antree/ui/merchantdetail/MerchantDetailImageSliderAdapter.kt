package com.commerce.antree.ui.merchantdetail

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.commerce.antree.R
import com.commerce.antree.appintro.AppIntroFragment

class MerchantDetailImageSliderAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm!!) {

    private val images = listOf(
        R.drawable.merchant_ichiban,
        R.drawable.ichiban_b,
        R.drawable.ichiban_c
    )

    override fun getItem(p0: Int): Fragment {
        return MerchantDetailImageSliderFragment.newInstance(images[p0])!!
    }

    override fun getCount(): Int {
        return images.size
    }

}