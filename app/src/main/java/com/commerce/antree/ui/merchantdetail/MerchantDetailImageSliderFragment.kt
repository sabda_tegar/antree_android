package com.commerce.antree.ui.merchantdetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.commerce.antree.R
import com.commerce.antree.appintro.AppIntroFragment
import kotlinx.android.synthetic.main.fragment_app_intro.*
import kotlinx.android.synthetic.main.fragment_merchant_detail_image_slider.*

class MerchantDetailImageSliderFragment: Fragment() {

    var images: Int? = null
    lateinit var baseView: View

    companion object {
        fun newInstance(image: Int) : Fragment? {
            val bundle = Bundle()
            bundle.putInt("image", image)

            val fragment = AppIntroFragment()
            fragment.setArguments(bundle)

            return fragment
        }
    }

    private fun readBundle(bundle: Bundle?) {
        if (bundle != null) {
            images = bundle.getInt("image")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_merchant_detail_image_slider, container, false)
    }

    @SuppressLint("NewApi")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        readBundle(arguments)

        context?.let {
            Glide.with(it)
                .load(images)
                .into(img_merchandetailimageslider_photoplace)
        }
    }
}
