package com.commerce.antree.ui.merchantdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.commerce.antree.data.repository.MainRepository
import com.commerce.antree.util.Resources
import kotlinx.coroutines.Dispatchers

class MerchantDetailViewModel(private val mainRepository: MainRepository) : ViewModel() {
    // TODO: Implement the ViewModel
    fun getPlaceByID(merchandId: String) = liveData(Dispatchers.IO) {
        emit(Resources.loading(data = null))
        try {
            emit(Resources.success(data = mainRepository.getPlaceByID(merchandId)))
        } catch (exception: Exception) {
            emit(Resources.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}