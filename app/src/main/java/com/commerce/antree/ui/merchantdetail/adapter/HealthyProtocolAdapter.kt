package com.commerce.antree.ui.merchantdetail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.commerce.antree.R
import com.commerce.antree.ui.home.data.MerchantCategory
import com.commerce.antree.ui.merchantdetail.data.HealthyProtocol
import kotlinx.android.synthetic.main.item_healthy_protocol.view.*
import kotlinx.android.synthetic.main.item_merchant_category.view.*

class HealthyProtocolAdapter(private val healthyProtocol: List<HealthyProtocol>) : RecyclerView.Adapter<HealthyProtocolHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): HealthyProtocolHolder {
        return HealthyProtocolHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_healthy_protocol, viewGroup, false))
    }

    override fun getItemCount(): Int = healthyProtocol.size

    override fun onBindViewHolder(holder: HealthyProtocolHolder, position: Int) {
        holder.bindHero(healthyProtocol[position])
    }
}


class HealthyProtocolHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvHealthProtocolDesc = view.txt_healthyprotocol_desc

    fun bindHero(healthyProtocol: HealthyProtocol) {
        tvHealthProtocolDesc.text = healthyProtocol.desc
//        itemView?.let {
//            Glide.with(it)
//                .load(merchantCategory.icon)
//                .into(imgMerchantIcon)
//        }
    }
}
