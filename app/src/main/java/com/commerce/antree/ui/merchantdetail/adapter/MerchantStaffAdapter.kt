package com.commerce.antree.ui.merchantdetail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.commerce.antree.R
import com.commerce.antree.ui.merchantdetail.data.MerchantStaff
import kotlinx.android.synthetic.main.item_merchant_category.view.*
import kotlinx.android.synthetic.main.item_merchant_staff.view.*

class MerchantStaffAdapter(private val merchantStaff: List<MerchantStaff>) : RecyclerView.Adapter<MerchantStaffViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MerchantStaffViewHolder {
        return MerchantStaffViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_merchant_staff, viewGroup, false))
    }

    override fun getItemCount(): Int = merchantStaff.size

    override fun onBindViewHolder(holder: MerchantStaffViewHolder, position: Int) {
        holder.bindHero(merchantStaff[position])
    }
}


class MerchantStaffViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvMerchantStaffName = view.txt_merchantstaff_name
    private val imgMerchantStaffPhotoProfile = view.img_merchantstaff_photoprofile
    private val tvMerchantStaffJobDesc = view.txt_merchantstaff_jobdesc

    fun bindHero(merchantStaff: MerchantStaff) {
        tvMerchantStaffName.text = merchantStaff.name
        tvMerchantStaffJobDesc.text = merchantStaff.jobDesc
        itemView?.let {
            Glide.with(it)
                .load(merchantStaff.photoProfile)
                .transform(CircleCrop())
                .into(imgMerchantStaffPhotoProfile)
        }
    }
}
