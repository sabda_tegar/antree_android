package com.commerce.antree.ui.merchantdetail.data

data class HealthyProtocol(var doing: Boolean = false, var desc: String)