package com.commerce.antree.ui.merchantdetail.data

data class MerchantStaff(var id: String, var photoProfile: Int, var name: String, var jobDesc: String)