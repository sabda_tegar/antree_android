package com.commerce.antree.ui.merchantgallery

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.commerce.antree.R
import com.commerce.antree.ui.merchantgallery.adapter.MerchantGalleryAdapter
import com.commerce.antree.ui.merchantgallery.data.MerchantGallery
import kotlinx.android.synthetic.main.merchant_gallery_fragment.*
import kotlinx.android.synthetic.main.queue_merchant_fragment.*

class MerhantGalleryFragment : Fragment() {

    companion object {
        fun newInstance() = MerhantGalleryFragment()
    }

    private lateinit var viewModel: MerchantGalleryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.merchant_gallery_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MerchantGalleryViewModel::class.java)
        setToolbar()
        val merchantGalleriesLastUpdate: List<MerchantGallery> = listOf(
            MerchantGallery("1", R.drawable.ichiban_f),
            MerchantGallery("2", R.drawable.ichiban_g)
        )
        // TODO: Use the ViewModel
        val merchantGalleries: List<MerchantGallery> = listOf(
            MerchantGallery("1", R.drawable.ichiban_a),
            MerchantGallery("2", R.drawable.ichiban_b),
            MerchantGallery("3", R.drawable.ichiban_c),
            MerchantGallery("4", R.drawable.ichiban_d),
            MerchantGallery("5", R.drawable.ichiban_e),
            MerchantGallery("6", R.drawable.ichiban_f),
            MerchantGallery("7", R.drawable.ichiban_g),
            MerchantGallery("9", R.drawable.merchant_ichiban),
            MerchantGallery("10", R.drawable.dinasty)
        )

        val merchantGalleryAdapterLastUpdate: MerchantGalleryAdapter = MerchantGalleryAdapter((merchantGalleriesLastUpdate))
        recyclerview_merchantgallery_photolastupdate.setLayoutManager(GridLayoutManager(activity, 2))
        recyclerview_merchantgallery_photolastupdate.adapter = merchantGalleryAdapterLastUpdate

        val merchantGalleryAdapter: MerchantGalleryAdapter = MerchantGalleryAdapter((merchantGalleries))
        recyclerview_merchantgallery_photo.setLayoutManager(GridLayoutManager(activity, 4))
        recyclerview_merchantgallery_photo.adapter = merchantGalleryAdapter
    }

    private fun setToolbar() {

        if(activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(toolbar_merchantgallery)
            (activity as AppCompatActivity).supportActionBar?.title= ""
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }
}