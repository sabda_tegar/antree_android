package com.commerce.antree.ui.merchantgallery.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.commerce.antree.MerchantActivity
import com.commerce.antree.R

import com.commerce.antree.ui.merchantgallery.data.MerchantGallery
import kotlinx.android.synthetic.main.item_merchant_category.view.*
import kotlinx.android.synthetic.main.item_merchant_gallery.view.*

class MerchantGalleryAdapter(private val merchantGallery: List<MerchantGallery>) : RecyclerView.Adapter<MerchantGalleryHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MerchantGalleryHolder {
        return MerchantGalleryHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_merchant_gallery, viewGroup, false))
    }

    override fun getItemCount(): Int = merchantGallery.size

    override fun onBindViewHolder(holder: MerchantGalleryHolder, position: Int) {
        holder.bindHero(merchantGallery[position])
    }
}

class MerchantGalleryHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val imgMerchantPhoto = view.img_merchantgallery_photo

    fun bindHero(merchantGallery: MerchantGallery) {
        itemView?.let {
            Glide.with(it)
                .load(merchantGallery.photo)
                .transform(CenterCrop())
                .into(imgMerchantPhoto)
        }

        /*itemView.setOnClickListener {
            val intent = Intent(it.context, MerchantActivity::class.java)
            it.context.startActivity(intent)
        }*/
    }
}