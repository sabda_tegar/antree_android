package com.commerce.antree.ui.merchantgallery.data

data class MerchantGallery(var id: String, var photo: Int)