package com.commerce.antree.ui.merchantreviewed

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.commerce.antree.R
import com.commerce.antree.ui.merchantreviewed.adapter.MerchantReviewedAdapter
import com.commerce.antree.ui.merchantreviewed.data.MerchantReviewed
import kotlinx.android.synthetic.main.merchant_reviewed_fragment.*

class MerchantReviewedFragment : Fragment() {

    companion object {
        fun newInstance() = MerchantReviewedFragment()
    }

    private lateinit var viewModel: MerchantReviewedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.merchant_reviewed_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MerchantReviewedViewModel::class.java)
        // TODO: Use the ViewModel
        val listMerchantReviewed: List<MerchantReviewed> = listOf(
            MerchantReviewed("Sabda Tegar", 4.0, "Tempatnya menyediakan tempat cuci tangan, jarak antar orang yang cukup, serta penggunaan masker bagi seluruh karyawan.", R.drawable.profile_tegar),
            MerchantReviewed("Ijal", 5.0, "Bagus, saya merasa aman untuk makan disini lagi selanjutnya.", R.drawable.profile_ijal),
            MerchantReviewed("Anti", 4.5, "Sirkulasi udara lebih diperbaiki.", R.drawable.profile_anti)
        )

        val merchantReviewedAdapter = MerchantReviewedAdapter(listMerchantReviewed)
        recyclerview_merchantreviewed_ratingandfeedback.setLayoutManager(LinearLayoutManager(activity))
        recyclerview_merchantreviewed_ratingandfeedback.adapter = merchantReviewedAdapter
    }

}