package com.commerce.antree.ui.merchantreviewed.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.commerce.antree.R
import com.commerce.antree.ui.merchantreviewed.data.MerchantReviewed
import kotlinx.android.synthetic.main.item_merchant_reviewed.view.*
import kotlinx.android.synthetic.main.item_merchant_staff.view.*

class MerchantReviewedAdapter(private val merchantReviewed: List<MerchantReviewed>) : RecyclerView.Adapter<MerchantReviewedViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MerchantReviewedViewHolder {
        return MerchantReviewedViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_merchant_reviewed, viewGroup, false))
    }

    override fun getItemCount(): Int = merchantReviewed.size

    override fun onBindViewHolder(holder: MerchantReviewedViewHolder, position: Int) {
        holder.bindHero(merchantReviewed[position])
    }
}


class MerchantReviewedViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvMerchantReviewedName = view.txt_merchantreviewed_username
    private val imgMerchantStaffPhotoProfile = view.img_merchantreviewed_userprofile
    private val tvMerchantReviewedRating = view.txt_merchantreviewed_userrating
    private val ratingMerchantReviewedRating = view.ratingbar_merchantreviewed_userrating
    private val tvMerchantReviewedFeedback = view.txt_merchantreviewed_userfeedback

    fun bindHero(merchantReviewed: MerchantReviewed) {
        tvMerchantReviewedName.text = merchantReviewed.name
        tvMerchantReviewedFeedback.text = merchantReviewed.feedback
        ratingMerchantReviewedRating.rating = merchantReviewed.rating.toFloat()
        tvMerchantReviewedRating.text = merchantReviewed.rating.toString() + " Stars"
        itemView?.let {
            Glide.with(it)
                .load(merchantReviewed.profilepicture)
                .transform(CircleCrop())
                .into(imgMerchantStaffPhotoProfile)
        }
    }
}
