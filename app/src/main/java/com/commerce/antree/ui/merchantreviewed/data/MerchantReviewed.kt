package com.commerce.antree.ui.merchantreviewed.data

data class MerchantReviewed (var name: String, var rating: Double, var feedback: String, var profilepicture: Int)