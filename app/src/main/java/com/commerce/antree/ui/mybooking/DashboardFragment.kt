package com.commerce.antree.ui.mybooking

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.commerce.antree.R
import com.commerce.antree.appintro.AppIntroActivity
import com.commerce.antree.ui.mybooking.adapter.MyBookingAdapter
import com.commerce.antree.ui.mybooking.data.MyBooking
import kotlinx.android.synthetic.main.fragment_dashboard.*

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val textView: TextView = root.findViewById(R.id.text_dashboard)
        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        val recyclerViewMyBooking: RecyclerView = root.findViewById(R.id.recyclerview_mybooking_merchantbooked)

        val myBookings: List<MyBooking> = listOf(
            MyBooking("1", R.drawable.merchant_ichiban, "Ichiban Sushi Pontianak", "Restaurant", 1)
        )

        val myBookingAdapter = MyBookingAdapter(myBookings)
        recyclerViewMyBooking.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false))
        recyclerViewMyBooking.adapter = myBookingAdapter

        Handler().postDelayed({
            val myBookings: List<MyBooking> = listOf(
                MyBooking("1", R.drawable.merchant_ichiban, "Ichiban Sushi Pontianak", "Restaurant", 5)
            )

            val myBookingAdapter = MyBookingAdapter(myBookings)
            recyclerViewMyBooking.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false))
            recyclerViewMyBooking.adapter = myBookingAdapter
        }, 30000)

        return root
    }
}