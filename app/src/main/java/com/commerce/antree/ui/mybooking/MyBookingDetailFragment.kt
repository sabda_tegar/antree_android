package com.commerce.antree.ui.mybooking

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.commerce.antree.R

class MyBookingDetailFragment : Fragment() {

    companion object {
        fun newInstance() = MyBookingDetailFragment()
    }

    private lateinit var viewModel: MyBookingDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.my_booking_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MyBookingDetailViewModel::class.java)
        // TODO: Use the ViewModel
    }

}