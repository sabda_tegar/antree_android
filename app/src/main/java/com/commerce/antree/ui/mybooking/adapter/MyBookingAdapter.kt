package com.commerce.antree.ui.mybooking.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.commerce.antree.GiveFeedbackToMerchantActivity
import com.commerce.antree.MerchantDetailActivity
import com.commerce.antree.R
import com.commerce.antree.ui.home.data.MerchantPlace
import com.commerce.antree.ui.mybooking.data.MyBooking
import kotlinx.android.synthetic.main.item_merchant.view.*
import kotlinx.android.synthetic.main.item_merchant_place.view.*
import kotlinx.android.synthetic.main.item_merchant_place.view.img_merchantplace_picture
import kotlinx.android.synthetic.main.item_my_booking.view.*

class MyBookingAdapter(private val myBooking: List<MyBooking> ) : RecyclerView.Adapter<MyBookingHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MyBookingHolder {
        return MyBookingHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_my_booking, viewGroup, false))
    }

    override fun getItemCount(): Int = myBooking.size

    override fun onBindViewHolder(holder: MyBookingHolder, position: Int) {
        holder.bindHero(myBooking[position])
    }
}


class MyBookingHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvMerchantName = view.txt_mybooking_merchanname
    private val tvStatus = view.txt_mybooking_status
    //private val imgMerchantPhoto = view.img_mybooking_merchantphoto

    fun bindHero(myBooking: MyBooking) {
        tvMerchantName.text = myBooking.merchantName
        when (myBooking.status) {
            1 -> tvStatus.text = "Menunggu"
            5 -> tvStatus.text = "Mohon Review"
        }
//        itemView?.let {
//            Glide.with(it)
//                .load(myBooking.merchantPhoto)
//                .into(imgMerchantPhoto)
//        }

        itemView.setOnClickListener {
            val intent = Intent(it.context, MerchantDetailActivity::class.java)
            intent.putExtra("booking", "yes")
            intent.putExtra("status", myBooking.status)
            it.context.startActivity(intent)
        }
    }
}
