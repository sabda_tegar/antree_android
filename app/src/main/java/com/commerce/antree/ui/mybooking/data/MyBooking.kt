package com.commerce.antree.ui.mybooking.data

data class MyBooking(var id: String, var merchantPhoto: Int, var merchantName: String, var merchantCategory: String, var status: Int = 0)