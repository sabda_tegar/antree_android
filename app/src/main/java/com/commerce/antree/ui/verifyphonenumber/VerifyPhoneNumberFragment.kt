package com.commerce.antree.ui.verifyphonenumber

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.commerce.antree.R
import com.commerce.antree.appintro.AppIntroActivity
import kotlinx.android.synthetic.main.verify_phone_number_fragment.*

class VerifyPhoneNumberFragment : Fragment() {

    companion object {
        fun newInstance() = VerifyPhoneNumberFragment()
    }

    private lateinit var viewModel: VerifyPhoneNumberViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.verify_phone_number_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VerifyPhoneNumberViewModel::class.java)
        // TODO: Use the ViewModel
        btn_verifyphonenumber_sendotp.setOnClickListener {
            if (edt_verifyphonenumber_phonenumber.text.toString().length < 1) {
                txt_verifyphonenumber_phonenumber.error = "this field is reuqired"
            } else {
                txt_verifyphonenumber_code.visibility = View.VISIBLE
            }
        }

        edt_verifyphonenumber_phonenumber.addTextChangedListener(textWatcher)

        btn_verifyphonenumber_sendotp.setOnClickListener {
            btn_verifyphonenumber_sendotp.visibility = View.GONE
            progress_sendotp.visibility = View.VISIBLE
            txt_verifyphonenumber_code.visibility = View.VISIBLE
            Handler().postDelayed({
                btn_verifyphonenumber_verify.visibility = View.VISIBLE
                progress_sendotp.visibility = View.GONE
            }, 4000)
        }
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            txt_verifyphonenumber_sendto.text = "Send to (+62) $s"
        }
    }
}