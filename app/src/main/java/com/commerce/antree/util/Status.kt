package com.commerce.antree.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}